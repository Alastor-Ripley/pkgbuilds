---
stages: [checks, update-pkgbuilds, bump-version, deploy]

variables:
  BUILD_HOST: builds.garudalinux.org
  BUILD_PORT: 223
  BUILD_USER: gitlab
  GIT_AUTHOR_EMAIL: ci@garudalinux.org
  GIT_AUTHOR_NAME: GitLab CI
  GIT_STRATEGY: clone
  REPO_URL: $CI_SERVER_PROTOCOL://gitlab-ci-token:$ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git

check-lint:
  # Check the PKGBUILD and install files for common bash anti-patterns and issues
  stage: checks
  image: alpine:latest
  inherit:
    variables: false
  script:
    - apk add --no-cache --upgrade bash npm py3-pip shellcheck shfmt
    - pip install yamllint
    - npm install -g markdownlint-cli
    - bash .ci/lint.sh
  rules:
    - if: $BUMPVER != "1"

check-pr-commitizen:
  # Check the current commit message for compliance with commitizen
  stage: checks
  image: alpine:latest
  inherit:
    variables: false
  script:
    - apk add --no-cache --upgrade py3-pip
    - pip install -U commitizen
    - cz check --message "$CI_COMMIT_MESSAGE" >/tmp/cz_check || true # why does it return 1 if its actually 0?
    - grep "successful" /tmp/cz_check # ugly hack to workaround the above issue
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

check-pr-makepkg:
  # Check the current commit for whether it builds
  stage: checks
  image: archlinux:latest
  inherit:
    variables: false
  script:
    - pacman -Syu --noconfirm base-devel git sudo
    - pacman-key --init && pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
    - pacman-key --lsign-key 3056513887B78AEB
    - pacman --noconfirm -U 'https://geo-mirror.chaotic.cx/chaotic-aur/chaotic-'{keyring,mirrorlist}'.pkg.tar.zst'
    - echo "[multilib]" >>/etc/pacman.conf
    - echo "Include = /etc/pacman.d/mirrorlist" >>/etc/pacman.conf
    - echo -e "[garuda]\\nInclude = /etc/pacman.d/chaotic-mirrorlist\\n[chaotic-aur]\\nInclude = /etc/pacman.d/chaotic-mirrorlist" >>/etc/pacman.conf
    - pacman -Syy
    - bash ./.ci/what-to-deploy.sh
    - bash ./.ci/test-build.sh
  artifacts:
    name: $CI_JOB_NAME-$CI_COMMIT_REF_NAME
    when: on_failure
    paths:
      - "*/*.log"

  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_COMMIT_MESSAGE =~ /\[deploy.*\]/

update-pkgbuilds:
  # This basically updates the version in the PKGBUILD and commits it,
  # triggering an instant build of the package via commit message.
  stage: update-pkgbuilds
  image: archlinux:latest
  script:
    - pacman -Syu --noconfirm git jq pacman-contrib python-pipx shfmt sudo
    - pipx install commitizen
    - git config --global user.name "$GIT_AUTHOR_NAME"
    - git config --global user.email "$GIT_AUTHOR_EMAIL"
    - bash .ci/version-bump.sh
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $BUMPVER == "1"

update-versions:
  # Update the VERSIONS file if needed, not triggering any of the pipelines with it
  image: alpine:latest
  stage: bump-version
  script:
    - apk add --no-cache --upgrade bash git
    - git config --global user.name "$GIT_AUTHOR_NAME"
    - git config --global user.email "$GIT_AUTHOR_EMAIL"
    - bash .ci/update-versions.sh
  rules:
    - changes:
        - "*/PKGBUILD"
      if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:
  # This only runs on the default branch and when the commit message contains [deploy *]
  # and is used to deploy the package to the build server.
  stage: deploy
  image: alpine:latest
  script:
    - '[[ -z ${DEPLOY_KEY+x} ]] && echo "No deploy key available, backing off!" && exit 1'
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$DEPLOY_KEY" >>~/.ssh/id_ed25519
    - chmod 400 ~/.ssh/id_ed25519
    - apk add --no-cache --upgrade bash openssh
    - bash .ci/what-to-deploy.sh
    - bash .ci/deploy.sh
  artifacts:
    name: $CI_JOB_NAME-$CI_COMMIT_REF_NAME
    when: on_failure
    paths:
      - "*.log"
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /\[deploy.*\]/ && $BUMPVER != "1"
